package com.example.jwt.service;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

@Service
public class FileServiceImpl implements FileService {
    private final Path path = Paths.get("src/main/resources/images");

    @Override
    public String uploadFile(MultipartFile file) throws Exception {
        try{
            String fileName = file.getOriginalFilename();
//            System.out.println(fileName);
            if (fileName != null && fileName.contains(".jpg") || fileName.contains(".png") || fileName.contains(".jpeg") || fileName.contains(".gif")){
                fileName = UUID.randomUUID() + "." + StringUtils.getFilenameExtension(fileName);
                if (!Files.exists(path)){
                    Files.createDirectories(path);
                }
                Files.copy(file.getInputStream(), path.resolve(fileName), StandardCopyOption.REPLACE_EXISTING);
                return fileName;
            } else {
                return "File Not Found";
            }
        }catch (Exception e){
            throw new Exception(e.getMessage());
        }
    }

    @Override
    public Resource getFile(String fileName) throws IOException {
        Path resourcePath = Paths.get("src/main/resources/images/" + fileName);
        Resource resource = new ByteArrayResource(Files.readAllBytes(resourcePath));
        return resource;
    }
}
