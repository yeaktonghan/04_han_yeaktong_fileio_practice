package com.example.jwt.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FileResponse<T> {
    private String message;
    private Integer responseCode;
    private T payload;
}
