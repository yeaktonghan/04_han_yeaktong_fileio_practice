package com.example.jwt.controller;

import com.example.jwt.model.FileResponse;
import com.example.jwt.service.FileService;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
public class FileController {

    private final FileService fileService;

    public FileController(FileService fileService) {
        this.fileService = fileService;
    }

    //    src/main/resources/images
    @PostMapping(value = "file/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> upload(@RequestParam MultipartFile file) throws Exception {
//        System.err.println(file.getOriginalFilename());
        String fileName = fileService.uploadFile(file);
        return ResponseEntity.ok(new FileResponse<>("Success", 200, fileName));
    }

    @GetMapping(value = "/file/get")
    public ResponseEntity<?> getFile(@RequestParam String fileName) throws IOException {
        Resource resource = fileService.getFile(fileName);
        return ResponseEntity.ok().contentType(MediaType.IMAGE_PNG).body(resource);
    }
}
